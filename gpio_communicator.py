import RPi.GPIO as GPIO
import time

from substatechecker import SubStateCheckThread, thread

RESET_BUTTON_PIN = 12
BUTTON_PRESSED_SIGNAL = 1

class GPIOCommunicator(object):

    def __init__(self):
        self.down_press_time = None
        self.release_time = None
        self.threader = SubStateCheckThread(.1)
        self.button_press_registered = False

    def setup_pins(self):
        GPIO.setmode(GPIO.BOARD)
        GPIO.setup(RESET_BUTTON_PIN, GPIO.IN)

    def set_reset_callback(self, callback, minimum_downtime, time_between_presses):

        print("Setting button callback")

        def check_button():

            button_state = GPIO.input(RESET_BUTTON_PIN)

            if button_state == BUTTON_PRESSED_SIGNAL:

                if self.release_time != None:
                    print("Resetting button press time")
                    self.release_time = None # Reset release time

                if not self.button_press_registered:
                    if self.down_press_time == None:
                        print("Setting button press time")
                        self.down_press_time = time.time()
                    else: # down_press_time was set earlier, and is still down
                        pressed_in_for = (time.time() - self.down_press_time)
                        print(f"After {pressed_in_for}")
                        if pressed_in_for > minimum_downtime:
                            callback()
                            self.button_press_registered = True # avoid multiple calls to callback even though button was only pressed once

            else:
                if self.down_press_time != None:
                    print("Resetting button press time")
                    self.down_press_time = None # Reset button time, because a low value was read for button press

                if not self.release_time:
                    print("Setting release time")
                    self.release_time = time.time()
                else:
                    released_for = (time.time() - self.release_time)
                    if released_for > time_between_presses:
                        self.button_press_registered = False

        self.threader.add_callback("button_check", check_button)
        if not self.threader.is_alive():
            self.threader.start()

    def is_button_pressed(self):
        return GPIO.input(RESET_BUTTON_PIN)
