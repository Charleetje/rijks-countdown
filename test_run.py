import sys
from GPIOEmulator import GPIO

class placeholder(object):
    GPIO = None

placeholder.GPIO = GPIO
sys.modules['RPi'] = placeholder
sys.modules['RPi.GPIO'] = GPIO

from run import *

if __name__=="__main__":
    socketio.run(app, debug=True)
