#!/bin/bash

# bash script to start oven code
# to have script start up on startup, add the following line to /etc/xdg/lxsession/LXDE-pi/autostart
# @sh /home/pi/rijks-countdown/startup.sh 

cd /home/pi/rijks-oven/
lxterminal -e "python3 /home/pi/rijks-countdown/run.py" &

# No screensaver
@xset s noblank
@xset s off
@xset -dpms

# Remove mouse after .1 seconds
@unclutter -idle 0.1 -roo
 
# If Chrome crashes (usually due to rebooting), clear the crash flag so we don't have the annoying warning bar
sed -i 's/"exited_cleanly":false/"exited_cleanly":true/' /home/pi/.config/chromium/Default/Preferences
sed -i 's/"exit_type":"Crashed"/"exit_type":"Normal"/' /home/pi/.config/chromium/Default/Preferences
 
# Run Chromium and open tabs
/usr/bin/chromium-browser --kiosk http://localhost:5000

cd /
