
const TOTAL_TIME = 12 * 60; // 20 minutes in seconds

// globals used
var time_left;
var interval;
var StateInstance;
var last_audio;

function State() {
  
  let ButtonActions = [
    "RESET",
    "START",
    "WIN"
  ];

  currentState = 1; // FIRST START TIMER WITH COUNTING DOWN

  this.getState = function ()
  {
    return ButtonActions[currentState];
  }

  this.incrementState = function()
  {
    currentState = (currentState + 1) % ButtonActions.length;
  }

  this.resetState = function()
  {
    currentState = 0;
  }

}

function pad_with_seethrough(number) {
  return (number < 10 ? '<span style="opacity: 0">0</span>' : '') + number  
}

function pad_with_zero(number) {
  return (number < 10 ? '0' : '') + number  
}


function fill_in_time(time_left)
{
  $('#minutes').html(pad_with_seethrough(Math.floor(time_left  / 60 )));
  $('#seconds').html(pad_with_zero(Math.floor(time_left ) % 60));
}

function set_countdown_interval(end_callback)
{
    interval = setInterval( function() {
      
      if (time_left > 0)
      {
        time_left--;
        fill_in_time(time_left);
      }
      else
      {
        // Remove countdown interval, because we are not counting down anymore
        clearInterval(window.interval);

        // Call the function that was requested to be called when the timer finishes
        end_callback();
      }

    }, 1000); // Set interval for every 1000 milliseconds
}

function play_audio(which_file)
{
  console.log("Playing audio", which_file);
  // var media = document.getElementById(which_file);
  window.last_audio = new Audio( '/static/media/' + which_file + '.mp3');
  const playPromise = last_audio.play();
  if (playPromise !== null){
    playPromise.catch(() => {
      console.log("Error caught");
      last_audio.play()
      .catch(function() {console.log("Errored again");}); });}
}

function do_button_action()
{
  // Make sure intervals set earlier are removed
  clearInterval(window.interval);

  let currentAction = StateInstance.getState();
  console.log(currentAction);

  if (currentAction === "RESET")
  {
    if (window.last_audio)
    {
      last_audio.pause();
    }

    time_left = TOTAL_TIME;
    fill_in_time(time_left);

    StateInstance.incrementState();
  }
  else if (currentAction === "START")
  {
    // Set countdown timer; when reaching the end (no time left),
    // the timer should be reset, so it skips the WIN state
    set_countdown_interval(StateInstance.resetState);

    StateInstance.incrementState();
  }
  else if (currentAction === "WIN")
  {
    play_audio("winning_sound_effect")
    StateInstance.resetState(); // Next button press should reset the timer
  }

}

$(document).ready(function() {
  let protocol = window.location.protocol;
  let socket = io.connect(protocol + '//' + document.domain + ':' + location.port);

  StateInstance = new State();

  socket.on('start_timer', function() { do_button_action(); });

  time_left = TOTAL_TIME;
  fill_in_time(TOTAL_TIME);
});