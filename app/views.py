from app import app
from flask import render_template

@app.route("/")
def first(methods=['GET']):
    return render_template("index.html")