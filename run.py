from flask_socketio import SocketIO
from app import app
from gpio_communicator import GPIOCommunicator
import time

MINIMUM_TIME_BUTTON_PRESSED_S = 1
TIME_BETWEEN_BUTTON_PRESSES_S = 0.5

app.config['SECRET_KEY'] = 'somesecretkey'
socketio = SocketIO(app)

gpio_communicator = GPIOCommunicator()
gpio_communicator.setup_pins()

def reset_callback():
    print("Received reset event")
    socketio.emit('start_timer', {}, broadcast=True)

gpio_communicator.set_reset_callback(reset_callback, MINIMUM_TIME_BUTTON_PRESSED_S, TIME_BETWEEN_BUTTON_PRESSES_S) 

@app.route('/start_timer')
def start_timer():
    print("starting timer")
    socketio.emit('start_timer', {}, broadcast=True)
    return('<h1>Timer started in other tab</h1>')

if __name__ == "__main__":

    socketio.run(app, debug=False)
